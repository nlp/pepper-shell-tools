# Pepper Shell Tools

various tools for working with the Pepper robot via command line shell

Developed at [NLP Centre](https://nlp.fi.muni.cz/en), [FI MU](https://www.fi.muni.cz/index.html.en) for [Karel Pepper](https://nlp.fi.muni.cz/projects/pepper)

## Installation

* copy the contents of `bin` directory to `/home/nao/bin` in the robot
* see the [NLP Pepper Tools page](https://nlp.fi.muni.cz/trac/pepper/wiki/NlpPepperTools) for description of individual tools
