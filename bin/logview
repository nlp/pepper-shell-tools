#!/usr/bin/perl -w

BEGIN {
    unshift @INC,"/home/nao/perl/lib/perl5/5.16.3/";
    # suppress 'Use of uninitialized value in numeric eq (==) at /home/nao/perl/lib/perl5/5.16.3//File/Tail.pm line 391.'
    $SIG{'__WARN__'} = sub { warn $_[0] unless (caller eq "File::Tail"); };
}
use File::Tail;
use Getopt::Long;
use Term::ANSIColor;
use POSIX qw{strftime};
use Sys::Hostname;

no warnings 'once';

$debug=undef;
$with_filename=undef;
$keep_also_nonfiltered=undef
$show_microseconds=undef;
$filter=undef;
$last_lines=undef;
$background_filter=1;
$pepper_hostname='Karel';
$pepper_ssh_name='karel';
GetOptions ("debug" => \$debug, "with-filename|H" => \$with_filename,
    "keep_also_nonfiltered" => \$keep_also_nonfiltered,
    "microseconds" => \$show_microseconds,
    "filter=s@" => \$filter,
    "background_filter!" => \$background_filter,
    "tail" => \$last_lines)
or die("Error in command line arguments\n");

if (hostname() ne $pepper_hostname) {
    exit (not(system("ssh $pepper_ssh_name /home/nao/bin/logview @ARGV")==0));
}
$background_filter='(HeadPositionAdjustment|NeckSetsuden|ExpressiveConfiguration|ALSignsAndFeedback.*Ears|ALSignsAndFeedback.*Eyes|ALSignsAndFeedback.*SpeechRecognitionBip)' if $background_filter;
@filters=();
if ($filter and @$filter) {
    for my $f (@$filter) {
        my ($grep,$color) = split(/,/,$f);
	if (defined($color) and $color) {
	    $color=~s/^bright *(\w)/bright_$1/;
	}
	else {
	    $color='bright_yellow';
	}
        push @filters, [$grep,$color];
    }
}

my @logfiles=();
for my $a (@ARGV) {
    if ($a =~ /^(dialog)$/) {
	push @logfiles,"/var/log/naoqi/servicemanager/system.Naoqi.log";
	push @logfiles,"/var/log/naoqi/servicemanager/system.Naoqi_error.log";
	push @filters, ['(dialog|audio|compile)','bold red'];
	push @filters, ['Word recognized: \[(.*?)\]','bold yellow'];
	push @filters, ['(czech|english)','bold white'];
    }
    elsif ($a =~ /^(detect)$/) {
	push @logfiles,"/var/log/naoqi/servicemanager/system.Naoqi.log";
	push @logfiles,"/var/log/naoqi/servicemanager/system.Naoqi_error.log";
	push @filters, ['(user|people|person)','bold red'];
	push @filters, ['detect\w+','bold yellow'];
    }
    else { push @logfiles,$a; }
}
if (not(@logfiles)) {
    push @logfiles,"/var/log/naoqi/tail-naoqi.log";
    push @logfiles, glob("/var/log/naoqi/servicemanager/*.log");
}
if ($last_lines) { $last_lines = 15; }
else { $last_lines=0; }
foreach (@logfiles) {
    my $ft = File::Tail->new(name=>"$_",debug=>$debug,maxinterval=>1,
	ignore_nonexistant=>1, tail=>$last_lines);
    $ft->{tail} = 0;
    push @files, $ft;
}
$keep_also_nonfiltered = 1 unless @filters;
$| = 1; # autoflush
$background_printed=0;
while (1) {
    $nfound=File::Tail::select(undef,undef,undef,60,@files);
    unless ($nfound) {
	my @ints;
	foreach(@files) {
	    push(@ints,$_->interval);
	}
	print "Nothing new! - ".localtime(time)."(".join(",",@ints).")\n";
    }
    foreach (@files) {
	next if $_->predict;
	my $text = $_->read;
	for my $line (split(/\r?\n/,$text)) {
	    if ($background_filter and $line=~/$background_filter/) {
		print ".";
		$background_printed = 1;
		next;
	    }
	    else {
		print "\n" if $background_printed; 
		$background_printed = 0;
	    }
	    my $l = process_line($_->{"input"}, $line);
	    print "$l\n" if $l;
	}
    }
}

sub colorize_line {
    my ($line) = @_;
    my @marks = ();
    for my $f (@filters) {
	my ($regex,$color) = @$f;
	$color = color('reset').color($color);
	while ($line =~ /$regex/ig) {
	    my $reset = undef;
	    # Scan match area to find last color
	    foreach my $i (reverse $-[0] .. $+[0]) {
		if (defined $marks[$i]) {
		    $reset = $marks[$i] unless defined $reset; 
		    $marks[$i] = undef; # Cancel previous color
		}
	    }
	    # If necessary, keep scanning to beginning of line
	    unless (defined $reset) {
		foreach my $i (reverse 0 .. $-[0]) {
		    if (defined $marks[$i]) {
			$reset = $marks[$i]; 
			last;
		    }
		}
	    }
	    # Mark area
	    $marks[$-[0]] = $color;
	    $marks[$+[0]] = $reset || color('reset');
	}  
    }
    # Apply color codes to the string
    foreach my $i (reverse 0 .. $#marks) {
	substr($line, $i, 0, $marks[$i]) if defined $marks[$i];
    }
    return ($line);
}

sub process_line {
    my ($file, $text) = @_;
    my $plain_line = $text;
    my $starting_line = 0;
    if ($plain_line =~ /^(\[[^]]+\]\s+)([0-9]+)\.([0-9]+)/) {
	$starting_line = 1;
	my ($flag,$seconds,$microseconds) = ($1,$2,$3);
	#my $stime = strftime '%a %b %e %H:%M:%S %Z %Y', localtime ($seconds);
	my $sdate = strftime '%b %e', localtime ($seconds);
	my $stime = strftime '%H:%M:%S', localtime ($seconds);
	if ($stime) {
	    my ($c1,$c2,$c3,$c4) = (color('yellow'),color('magenta'),color('green'),color('reset'));
	    if ($flag eq '[E] ') {
		($c1,$c2,$c3,$c4) = (color('red on_bright_yellow'),color('magenta on_bright_yellow'),color('green on_bright_yellow'),color('black on_bright_yellow'));
	    }
	    if ($show_microseconds) {
		$plain_line =~ s/^(\[[^]]+\]\s+)([0-9]+)\.([0-9]+)/$c1$flag$c2$sdate $c3$stime.$microseconds$c4/;
	    }
	    else {
		$plain_line =~ s/^(\[[^]]+\]\s+)([0-9]+)\.([0-9]+)/$c1$flag$c2$sdate $c3$stime$c4/;
	    }
	}
	if ($flag eq '[E] ') { $plain_line = $plain_line.color('reset'); }
	    
    }
    my $line = colorize_line($plain_line);
    return ('') if ($line eq $plain_line and not $keep_also_nonfiltered and $starting_line);
    $line = "$file: $line" if $with_filename;
    return ($line);
}
