#!/usr/bin/python
# -*- encoding: UTF-8 -*-

''' Simple TTS '''

import argparse
import qi
import sys

def main(ip, port, language, volume, tofile, text):
    s = qi.Session()
    s.connect("tcp://"+str(ip)+":"+str(port))
    # Get proxies
    tts = s.service("ALTextToSpeech")
    anim = s.service("ALAnimatedSpeech")
    audio = s.service("ALAudioDevice")

    #tts.addToDictionary("Karel", "\\toi=lhp\\Karel\\toi=orth\\")

    cur_lang = tts.getLanguage()
    if language is None:
        language = cur_lang
    if language != cur_lang: tts.setLanguage(language)
    if text == "": text = "Hello" 
    print "Text is '{}' (in {})".format(text, language)
    sys.stdout.flush()
    audio.setOutputVolume(volume)
    if tofile is not None:
        tts.sayToFile(text, tofile)
        print "Saved to '{}'".format(tofile)
    else:
        anim.say(text)
    if language != cur_lang: tts.setLanguage(cur_lang)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('text', nargs='*')
    parser.add_argument("--ip", type=str, default="127.0.0.1", help="Robot IP address")
    parser.add_argument("--port", type=int, default=9559, help="Robot port number")
    parser.add_argument("--language", type=str, help="Language to use (default depends on robot settings)")
    parser.add_argument("--file", type=str, help="Read the text from file")
    parser.add_argument("--tofile", type=str, help="Write the output sound to a WAV file")
    parser.add_argument("--volume", type=int, default=50, help="Set volume percentage")

    args = parser.parse_args()
    text = ' '.join(args.text)
    if args.file is not None:
        with open(args.file, 'r') as myfile:
            text = myfile.read() + text
    main(args.ip, args.port, args.language, args.volume, args.tofile, text)
