#!/usr/bin/python

import qi
import argparse
import sys
import os

def show_topic (topic):
    with open(topic) as file:  
        head = file.readlines()
    for line in head[:10]:
        sys.stdout.write("    "+line)
    if len(head)>10:
        print "    ... ({} more lines)".format(len(head)-10)

def main(session, language, topics):
    """
    this example shows the activateTopic method from the ALDialog API
    """
    ALDialog = session.service("ALDialog")
    curlang = ALDialog.getLanguage()
    ALDialog.setLanguage(language)
    loaded = []
    for topic in topics:
        topic_file = os.path.abspath(topic)
        topicName = ALDialog.loadTopic(topic_file)
        print "{} -> {}".format(topic_file,topicName)
        ALDialog.compileAll()
        ALDialog.activateTopic(topicName)
        show_topic(topic_file)
        loaded.append(topicName)
    # Starting the dialog engine - we need to type an arbitrary string as the identifier
    # We subscribe only ONCE, regardless of the number of topics we have activated
    ALDialog.subscribe('my_dialog_example')
    try:
        raw_input("\nSpeak to the robot using rules from the activated topic(s). Press Enter when finished:")
    finally:
        # stopping the dialog engine
        ALDialog.unsubscribe('my_dialog_example')
        for topic in loaded:
            ALDialog.deactivateTopic(topic)
            ALDialog.unloadTopic(topic)
        if curlang != language:
            ALDialog.setLanguage(curlang)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot's IP address. If on a robot or a local Naoqi - use '127.0.0.1' (this is the default value).")
    parser.add_argument("--port", type=int, default=9559,
                        help="port number, the default value is OK in most cases")
    parser.add_argument("--lang", type=str, default="English",
                        help="dialog language")
    parser.add_argument("topic", type=str, nargs='+',
                        help="topic(s)")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://{}:{}".format(args.ip, args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at IP {} (port {}).\nPlease check your script's arguments."
               " Run with -h option for help.".format(args.ip, args.port))
        sys.exit(1)
    main(session,args.lang, args.topic)

