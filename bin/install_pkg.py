#!/usr/bin/python

import qi
import warnings
# /usr/local/lib/python2.7/dist-packages/cryptography/__init__.py
# Python 2 is not supported warning
warnings.simplefilter("ignore", lineno=33)
import paramiko
import os
import sys
import argparse
import logging
import getpass


ROBOT_DEF_URL = "192.168.0.10"
ROBOT_DEF_SSH = "pepper"

INSTALL_LOG_FILE = "install.log"

def ssh_connect (ssh):
    client = paramiko.SSHClient()
    client._policy = paramiko.WarningPolicy()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ssh_config = paramiko.SSHConfig()
    cfg = None
    user_config_file = os.path.expanduser("~/.ssh/config")
    if os.path.exists(user_config_file):
        with open(user_config_file) as f:
            ssh_config.parse(f)
            cfg = {'hostname': ssh, 'username': "nao", 'allow_agent': False, 'look_for_keys': False}
            user_config = ssh_config.lookup(cfg['hostname'])
            for k in ('hostname', 'username', 'port', 'identityfile'):
                if k in user_config:
                    if k == 'identityfile':
                        cfg['key_filename'] = user_config['identityfile']
                    else:
                        cfg[k] = user_config[k]

                if 'proxycommand' in user_config:
                    cfg['sock'] = paramiko.ProxyCommand(user_config['proxycommand'])
    assert cfg is not None, "You should set SSH access to the robot"
    client.connect(**cfg)
    return (client)

def main(ip, port, ssh, listapps=False, removeapps=False, packages = []):

    if ssh is None: ssh = ip
    print "Connecting NAOqi session"
    app = qi.Application(url='tcp://'+ip+':'+str(port))
    app.start()
    session = app.session
    packagemgr = session.service("PackageManager")

    apps_data = []
    if listapps or removeapps:
        if listapps: print "Listing the current applications"
        else: print "Obtaining the list of applications"
        try:
            apps_data = packagemgr.packages()
            apps = ["\t{}\t{}".format(a['uuid'],a['version']) for a in apps_data]
            if listapps: print '\n'.join(apps)
            #print '  '+repr(apps_data)
        except Exception as err:
            print "error: {}".format(err)
    if not listapps or removeapps:
        sshc = ssh_connect(ssh)
        sftp = sshc.open_sftp()
        for pkg_file in packages:
            if removeapps:
                apps = [a['uuid'] for a in apps_data]
                if pkg_file in apps:
                    print ("Removing {}".format(pkg_file))
                    packagemgr.removePkg(pkg_file)
                else:
                    print "Package {} not installed".format(pkg_file)
            else:
                pkg_path = os.path.abspath(pkg_file)
                pkg_file = os.path.basename(pkg_file)
                print "Uploading PKG {} as {}".format(pkg_path,pkg_file)
                sftp.put(pkg_path, pkg_file)

                print "Installing app {}".format(pkg_file)
                packagemgr.install("/home/nao/"+pkg_file)

                print "Cleaning PKG"
                sftp.remove(pkg_file)

        print "End"
        sftp.close()
        sshc.close()

    app.stop()

if __name__ == "__main__":
    log_message = "command={}".format(sys.argv)
    try: 
        basedir = os.path.dirname(__file__)
        logging.basicConfig(filename=os.path.join(basedir,INSTALL_LOG_FILE),
                level=logging.INFO,
                format='%(asctime)-15s {} %(levelname)s %(message)s'.format(getpass.getuser()))
        #os.chmod(os.path.join(basedir,INSTALL_LOG_FILE), 0662)

        parser = argparse.ArgumentParser()
        parser.add_argument("--ip", type=str, default=ROBOT_DEF_URL, help="Robot IP address")
        parser.add_argument("--port", type=int, default=9559, help="Robot port number")
        parser.add_argument("--ssh", type=str, default=ROBOT_DEF_SSH, help="Robot SSH hostname")
        parser.add_argument("-l", "--list", action='store_true',
                            help="List installed applications")
        parser.add_argument("-r", "--remove", action='store_true',
                            help="Remove the listed application(s)/package(s)")
        parser.add_argument('packages', nargs='*', help="Package(s) to install")

        args = parser.parse_args()
        main(args.ip, args.port, args.ssh, args.list, args.remove, args.packages)
    finally:
        logging.info(log_message)

