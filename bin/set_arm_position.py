#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

"""Example : Set Pepper hand to specified position"""

import qi
import argparse
import almath
import time
import math
import motion
import tty, sys, termios, select

#offerArmPose = {
#    'ShoulderPitch': 0.86,
#    'ShoulderRoll': 0.43,
#    'ElbowYaw': -1.38,
#    'ElbowRoll': -0.81,
#    'WristYaw': -0.31,
#    'Hand': 0.54}
LArmofferArmPoseStr = '0.86,0.43,-1.38,-0.81,-0.31,0.54'
RArmofferArmPoseStr = '0.86,-0.43,1.38,0.81,0.31,-0.54'
offerArmPoseStr = LArmofferArmPoseStr
printCheckTime = 2.0
last_show_time = 0
last_angles_diff = []

def angles_different( angles_diff):
    global last_angles_diff

    round_angles = [round(a,2) for a in angles_diff]
    is_diff = (round_angles != last_angles_diff)
    last_angles_diff = round_angles
    return (is_diff)

def offerArm(motion_service, armAngles, showPos = 0, saveInitPos = False):
    global jointNames, armVar, armInitAngles, offerArmPosition, last_show_time

    angles = armAngles
    if ((showPos > 0 and (time.time() - last_show_time) > printCheckTime)) or showPos > 1:
        #print "offer{}Arm:\n  {}".format(armVar, ', '.join([x[0]+': '+str(x[1]) for x in zip(jointNames, angles)]))
        print "offer{}Arm:\n  Angles = {}".format(armVar, ', '.join(map(str, angles)))
    #motion_service.angleInterpolation(jointNames, angles, 0.5, True)
    motion_service.setAngles(jointNames, angles, 0.3)
    if saveInitPos:
        armInitAngles = angles
        offerArmPosition = motion_service.getPosition(armVar + "Hand", motion.FRAME_ROBOT, False)

    sensor_angles = motion_service.getAngles(jointNames, True)
    angles_diff = [x-angles[i] for i, x in enumerate(sensor_angles)]
    hand = armVar + "Hand"
    hand_pos_s = motion_service.getPosition(hand, motion.FRAME_ROBOT, True)
    hand_pos_diff = [x-offerArmPosition[i] for i, x in enumerate(hand_pos_s)]
    if (showPos > 0 and (angles_different(angles_diff) or \
    (time.time() - last_show_time) > printCheckTime)) or showPos > 1:
        print "  Angles {}iff = {}".format('D' if last_angles_diff != angles_diff else 'd', \
            ', '.join(["{:.2f}".format(x) for x in angles_diff]))
        pos6d = ['x', 'y', 'z', 'wx', 'wy', 'wz']
        print "  Init position diff of {} = {}".format(hand, ', '.join(["{}: {:.2f}".format(*x) for x in zip(pos6d, hand_pos_diff)]))
    last_show_time = time.time()

def has_ch():
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])

def init_getch():
    global old_term_settings
    old_term_settings = termios.tcgetattr(sys.stdin)
    try:
        tty.setcbreak(sys.stdin.fileno())
    except:
        done_getch()

def done_getch():
    global old_term_settings
    termios.tcsetattr(sys.stdin, termios.TCSADRAIN, old_term_settings)

def getch():
    ch = None
    while has_ch():
        ch = sys.stdin.read(1)
    return ch

def initRobotPosition(armAngles, motion_service, posture_service, aware_service, abackground_service):
    """
    Inits Pepper's position and stiffnesses to make the guiding possible.
    """
    global armVar

    motion_service.wakeUp()
    aware_service.pauseAwareness()
    abackground_service.setEnabled(False)
    posture_service.goToPosture("StandInit", 0.3)
    motion_service.moveInit()
    time.sleep(1.0)

    motion_service.setStiffnesses(armVar+'Arm', 1.0)
    motion_service.setAngles(armVar + "WristYaw", 0.0, 1.0)
    motion_service.setAngles("Head", [0.44, -0.44], 0.1)
    offerArm(motion_service, armAngles, True, True)


def main(session, armName, armAnglesStr, interactive):
    global jointNames, armVar, old_term_settings

    # Get the services ALMemory, ALMotion, ALRobotPosture and ALLeds.

    aware_service = session.service("ALBasicAwareness")
    abackground_service = session.service("ALBackgroundMovement")
    motion_service = session.service("ALMotion")
    posture_service = session.service("ALRobotPosture")

    armVar = 'L'
    if armName == "RArm":
        armVar = 'R'

    try:
        jointNames = motion_service.getBodyNames(armName)

        limits = motion_service.getLimits(armName)
        print "{}Arm limits:".format(armVar)
        for i in range(0,len(limits)):
            print "  {}. {}: angle=<{:+.2f}..{:+.2f}>, velocity=<0..{:.2f}>, torque=<0..{:.2f}>".format(\
                i+1, jointNames[i], \
                limits[i][0], limits[i][1], limits[i][2], limits[i][3])

        # Init robot position.

        angles = [float(x) for x in armAnglesStr.split(',')]
        initRobotPosition(angles, motion_service, posture_service, aware_service, abackground_service)
        if not interactive:
            offerArm(motion_service, angles)
            time.sleep(1)
            offerArm(motion_service, angles)
            time.sleep(1)
            offerArm(motion_service, angles)
            return

        print "Use '+', '-', '1'..'{}'. '0' to stop".format(len(jointNames))
        init_getch()
        ch = '1'
        activeJoint = 0
        while True:
            offerArm(motion_service,angles,1 if (ch is None) else 2)
            time.sleep(0.5)
            ch = getch()
            if ch is None:
                continue
            print "You pressed '{}'".format(ch)
            if ch == '0' or ch == 'q':
                break
            if ch >= '1' and ch <= chr(ord('0')+len(jointNames)):
                activeJoint = (ord(ch) - ord('1'))
                print "Active joint is {}: {}".format(activeJoint, jointNames[activeJoint])
            if ch == '+':
                angles[activeJoint] = angles[activeJoint] + 0.1
                if angles[activeJoint] > limits[activeJoint][1]:
                    angles[activeJoint] = limits[activeJoint][1]
            elif ch == '-':
                angles[activeJoint] = angles[activeJoint] - 0.1
                if angles[activeJoint] < limits[activeJoint][0]:
                    angles[activeJoint] = limits[activeJoint][0]
            if ch == '?' or ch == 'h':
                print "Use '+', '-', {}. '0' to stop".format(', '.join(["'{}':{}".format(i+1,x) for i, x in enumerate(jointNames)]))
    finally:
        print "Stopping..."
        if 'old_term_settings' in globals():
            done_getch()
        # Set LEDs to white.
        aware_service.resumeAwareness()
        abackground_service.setEnabled(True)

    # Crouch.
    #motion_service.rest()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")
    parser.add_argument("--arm", type=str, default="LArm",
                        help="Which arm to move")
    parser.add_argument("--angles", type=str, default=LArmofferArmPoseStr,
                        help="Set arm angles")
    parser.add_argument("--interactive", action='store_true',
                        help="Start interactive session")

    args = parser.parse_args()
    if args.arm == "RArm" and args.angles == LArmofferArmPoseStr:
        args.angles = RArmofferArmPoseStr

    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session, args.arm, args.angles.replace(' ',''), args.interactive)
