#!/usr/bin/python

import qi
import paramiko
import os
import sys
import argparse
import logging
import getpass
import re


# see https://stackoverflow.com/questions/47242888/what-is-the-difference-between-starting-choregraphe-application-with-a-trigger-s/47274956#47274956#answers
USE_AUTONOMOUSLIFE = True

ROBOT_DEF_URL = "192.168.0.10"
ROBOT_DEF_SSH = "pepper"

RUN_LOG_FILE = "run.log"

def ssh_connect (ssh):
    client = paramiko.SSHClient()
    client._policy = paramiko.WarningPolicy()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    ssh_config = paramiko.SSHConfig()
    user_config_file = os.path.expanduser("~/.ssh/config")
    if os.path.exists(user_config_file):
        with open(user_config_file) as f:
            ssh_config.parse(f)
            cfg = {'hostname': ssh, 'username': "nao", 'timeout': 10}
            user_config = ssh_config.lookup(cfg['hostname'])
            for k in ('hostname', 'username', 'port', 'identityfile'):
                if k in user_config:
                    if k == 'identityfile':
                        cfg['key_filename'] = user_config['identityfile']
                    else:
                        cfg[k] = user_config[k]

                if 'proxycommand' in user_config:
                    cfg['sock'] = paramiko.ProxyCommand(user_config['proxycommand'])
    client.connect(**cfg)
    return (client)

def main(ip, port, ssh, listbehavs=False, listapps=False, behaviors = []):

    if ssh is None: ssh = ip
    sshc = ssh_connect(ssh)
    print "Connecting NAOqi session"
    app = qi.Application(url='tcp://'+ip+':'+str(port))
    app.start()
    session = app.session
    packagemgr = session.service("PackageManager")
    behaviormgr = session.service("ALBehaviorManager")
    autonomouslife = session.service("ALAutonomousLife")

    if listbehavs:
        print "Listing the current behaviors"
        try:
            behavs = behaviormgr.getInstalledBehaviors()
            print '  '+'\n  '.join(behavs)
        except Exception as err:
            print "error: {}".format(err)
    elif listapps:
        print "Listing the current applications"
        try:
            apps_json = packagemgr.packages()
            apps = ["{}\t{}".format(a['uuid'],a['version']) for a in apps_json]
            print '  '+'\n  '.join(apps)
            #print '  '+repr(apps_json)
        except Exception as err:
            print "error: {}".format(err)
    else:
        for appname in behaviors:
            try:
                print "Starting the application {}".format(appname)
                if USE_AUTONOMOUSLIFE:
                    behaviormgr.startBehavior(appname)
                else:
                    autonomouslife.switchFocus(appname)
            except Exception as err:
                if re.search('is already running',str(err)):
                    print "Application {} is already running, stopping it now".format(appname)
                    try:
                        behaviormgr.stopBehavior(appname)
                        print "Stopped, starting the application {}".format(appname)
                        if USE_AUTONOMOUSLIFE:
                            behaviormgr.startBehavior(appname)
                        else:
                            autonomouslife.switchFocus(appname)
                    except Exception as err:
                        print "error: {}".format(err)
                else:
                    print "error: {}".format(err)
        print "End"
    sshc.close()
    app.stop()


if __name__ == "__main__":
    log_message = "command={}".format(sys.argv)
    try: 
        basedir = os.path.dirname(__file__)
        logging.basicConfig(filename=os.path.join(basedir,RUN_LOG_FILE),
                level=logging.INFO,
                format='%(asctime)-15s {} %(levelname)s %(message)s'.format(getpass.getuser()))
        #os.chmod(os.path.join(basedir,RUN_LOG_FILE), 0662)

        parser = argparse.ArgumentParser()
        parser.add_argument("--ip", type=str, default=ROBOT_DEF_URL, help="Robot IP address")
        parser.add_argument("--port", type=int, default=9559, help="Robot port number")
        parser.add_argument("--ssh", type=str, default=ROBOT_DEF_SSH, help="Robot SSH hostname")
        parser.add_argument("-l", "--list", action='store_true',
                            help="List installed behaviors")
        parser.add_argument("-a", "--applicationslist", action='store_true',
                            help="List installed applications")
        parser.add_argument('behaviors', nargs='*', help="Behavior(s) to run")

        args = parser.parse_args()
        main(args.ip, args.port, args.ssh, args.list, args.applicationslist, args.behaviors)
    finally:
        logging.info(log_message)

